<?php
namespace Folio\OauthClient;

use Folio\OauthClient\Client as ApiCurl;
/**
*
*/
class ResourceRequest
{
    protected $url    = '';
    protected $token  = '';
    protected $data   = array();
    protected $tokenFilename = 'app.key';

    public function __construct()
    {
        if (!function_exists('curl_version')) {
            die("Curl is not installed. Please install it before running this script.");
        }
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    // request token
    public function requestToken($data)
    {
        try {
            if (!$this->url) {
                throw new Exception("URL is not set.");
            }

            $api = new ApiCurl();
            $api->initCurl($this->url, 'POST')
                ->setData($data)
                ->setOption()
                ->run()
                ->closeCurl();

            return $api->getResult();
        } catch (Exception $e) {
            return json_encode(['error_code' => 0, 'description' => $e->getMessage()]);
        }
    }

    // a preflight validation of token
    public function authenticate($token)
    {
        try {
            if (!$this->url) {
                throw new Exception("URL is not set.");
            }

            $api = new ApiCurl();
            $api->initCurl($this->url, 'POST')
                ->setAuthHeader($token)
                ->setOption()
                ->run()
                ->closeCurl();

            return $api->getResult();

        } catch (Exception $e) {
            return json_encode(['error_code' => 0, 'description' => $e->getMessage()]);
        }
    }

    // request resource
    public function requestResource($data=null, $method="GET", $token=null)
    {
        try {
            if (!$this->url) {
                throw new Exception("URL is not set.");
            }

            $api = new ApiCurl();
            $api->initCurl($this->url, $method)
                ->setAuthHeader($token)
                ->setData($data)
                ->setOption()
                ->run()
                ->closeCurl();

            return $api->getResult();

        } catch (Exception $e) {
            return json_encode(['error_code' => 0, 'description' => $e->getMessage()]);
        }
    }
}