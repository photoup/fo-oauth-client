<?php
namespace Folio\OauthClient;

/**
* Request token and api resource.
*/
class Client
{
    protected $ch;
    protected $action;
    protected $token;
    protected $data;
    protected $header;
    protected $result;
    protected $referer;

    public function initCurl($url, $action="GET")
    {
        $this->ch     = curl_init($url);
        $this->action = $action;
        $this->header = array('Content-Type: application/json');

        return $this;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getAllCurlInfo()
    {
        return curl_getinfo($this->ch);
    }

    public function getCurlInfo()
    {
        return curl_getinfo($this->ch, CURLINFO_EFFECTIVE_URL);
    }

    public function setAuthHeader($token)
    {
        $this->token    = $token;
        $this->header[] = "Authorization: Bearer " . $this->token;

        return $this;
    }

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function setHeader($header=array())
    {
        $this->header = $header;

        return $this;
    }

    public function addHeader($header=array())
    {
        $this->header = array_merge($this->header, $header);

        return $this;
    }

    public function setOption()
    {
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $this->action);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_POST, true);
        curl_setopt($this->ch, CURLOPT_REFERER, $this->referer);

        if ($this->data) {
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($this->data));
        }

        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 300); //timeout after 30 seconds

        return $this;
    }

    public function run()
    {
        $this->result = curl_exec($this->ch);

        // Check the return value of curl_exec(), too
        if ($this->result === false) {
            throw new \Exception(curl_error($this->ch), curl_errno($this->ch));
        }

        return $this;
    }

    public function closeCurl()
    {
        curl_close($this->ch);
    }
}